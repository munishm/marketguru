//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

let mongoose = require("mongoose");
let stock = require('../server/models/stock');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../server');
let should = chai.should();

chai.use(chaiHttp);
//Our parent block
describe('Stock', () => {
    //Re-implement the logic for dev/production
    //run this test to empty the first occurance
    // beforeEach((done) => { //Before each test we empty the database
    //     stock.remove({}, (err) => {
    //         done();
    //     });
    // });
    /*
    * Test the /GET route
    */
    describe('/GET stock', () => {
        it('should GET the stock details', (done) => {
            chai.request(server)
                .get('/stock?function=TIME_SERIES_DAILY&symbol=MSFT&apikey=HZGSAGH6VAAO71CN')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    done();
                });
        });
        it('should error out if function query is not set in GET', (done) => {
            chai.request(server)
                .get('/stock?functio=TIME_SERIES_WEEKLY')
                .end((err, res) => {
                    res.should.have.status(422);
                    res.body.should.be.a('object');
                    res.body.should.have.property('errors');
                    done();
                });
        });
        it('should error out if symbol query is not set in GET', (done) => {
            chai.request(server)
                .get('/stock?function=TIME_SERIES_WEEKLY')
                .end((err, res) => {
                    res.should.have.status(422);
                    res.body.should.be.a('object');
                    res.body.should.have.property('errors');
                    done();
                });
        });
        it('should error out if apikey query is not set in GET', (done) => {
            chai.request(server)
                .get('/stock?function=TIME_SERIES_WEEKLY&symbol=MSFT')
                .end((err, res) => {
                    res.should.have.status(422);
                    res.body.should.be.a('object');
                    res.body.should.have.property('errors');
                    done();
                });
        });
        it('should contain property Time Series (Daily)', (done) => {
            chai.request(server)
                .get('/stock?function=TIME_SERIES_DAILY&symbol=MSFT&apikey=HZGSAGH6VAAO71CN')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('Time Series (Daily)');
                    done();
                });
        });
        it('should contain property Time Series (Weekly)', (done) => {
            chai.request(server)
                .get('/stock?function=TIME_SERIES_WEEKLY&symbol=MSFT&apikey=HZGSAGH6VAAO71CN')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('Weekly Time Series');
                    done();
                });
        });
        it('should contain MACD property', (done) => {
            chai.request(server)
                .get('/stock?function=MACD&symbol=MSFT&apikey=HZGSAGH6VAAO71CN')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('Technical Analysis: MACD');
                    done();
                });
        });
        //STUBS for other potential tests
        it("should check for error responses for Invalid symbols", (done) => {
            chai.request(server)
                .get('/stock?function=TIME_SERIES_DAILY&symbol=ABRAKADABRA&apikey=HZGSAGH6VAAO71CN')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('Error Message');
                    done();
                });
        });
    });

});