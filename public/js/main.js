
// Global
var apiKey = "HZGSAGH6VAAO71CN";


//If start date is null or whitespace make it the current date
if (!($('#StartDate').val())) {
    $('#StartDate').val(moment(Date.now()).subtract(365, "days").format('MM/DD/YYYY'));
}

//If no stock symbol selected select one
if (!($('#StockSymbol').val())) {
    $('#StockSymbol').val("MSFT");
}


$(document).ready(function () {

    //Initializing necessary modules
    CompanyInfo.init();
    UIBindings.init();
    stockLineGraph.init();
    MovingAvgConvDivgGraph.init();
});

