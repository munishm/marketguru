// #! javascript
var expect = chai.expect;

describe("Utils", function() {
    describe("Aha moments",function(){
        it("should get the minimum date", function(){
            var dateArr = ["1-2-2018","1-1-2018","1-3-2018"]
            expect(Utils.minDate(dateArr)).to.equal("1-1-2018");
        });  
        it("should check if array in ascending order", function(){
            var dataArr = [1,4,2,7,5]
            expect(Utils.isAscending(dataArr)).to.be.false;
        });
        it("should return true for array in ascending order", function(){
            var dataArr = [1,2,7,8,19]
            expect(Utils.isAscending(dataArr)).to.be.not.false;
        });

    });
});

describe("Chart tests", function(){
    describe("Line chart test",function(){
        //Stub for line chart internal methods
        it("should inititate the Line chart", function(){
            //initiate it and check for chart properties
            expect(true).to.be.true;
        });

    });
    describe("MACD chart test",function(){
        it("should inititate the MACD chart", function(){
             //initiate it and check for chart properties
            expect(true).to.be.true;
        });
        it("should check the bullish/bearish behavior of stock", function(){
            var mockMACDData= {};
            mockMACDData.datasets = [
                {
                    data:[]
                },
                {
                    data:["0.8366","0.9966","1.3308","1.3811","2.1933"]
                },
                {
                    data:["0.7182","0.7739","0.9548","1.1780","2.1869"]
                }
            ]; 
            expect(MovingAvgConvDivgGraph.shouldBuyStock(mockMACDData)).to.be.true;
        });

    });
})