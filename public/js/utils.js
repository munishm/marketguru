/* /js/Utils.js */

//Author: Munish Malhotra

var Utils = (function () {
    'use strict';
    var DOM = {};

    //========================================================================================
    //                                  PRIVATE METHODS
    //========================================================================================

    // cache DOM elements for internal use
    function cacheDom() {
        DOM.$symbol = $('#StockSymbol').val().toUpperCase();
        DOM.$startDate = $("#StartDate");
        DOM.$sellTicker = $('#BuySell');
        DOM.$MACDChart = $("#MACD-ChartDiv");
    }

    //TODO: Math.min.apply should be more elegant.
    //function to get min date from array
    function minDate(all_dates) {
        var min_dt = all_dates[0],
            min_dtObj = new Date(all_dates[0]);
        all_dates.forEach(function (dt, index) {
            if (new Date(dt) < min_dtObj) {
                min_dt = dt;
                min_dtObj = new Date(dt);
            }
        });
        return min_dt;
    };

    // function minDate(dates){
    //     let moments = this.dates.map(d => moment(d)),
    //     minDate = moment.min(moments);
    //     return minDate;
    // }

    //Check if array is Ascending
    function isAscending(arr) {
        return arr.every(function (x, i) {
            return i === 0 || x >= arr[i - 1];
        });
    }

    //get server URL 
    function getUrl(){
        return 'http://localhost:3001/stock'
;    }

    //========================================================================================
    //                                  PUBLIC METHODS
    //========================================================================================

    function init() {

    }

    //========================================================================================
    //                                  EXPORT PUBLIC METHODS
    //========================================================================================

    return {
        init: init,
        isAscending: isAscending,
        getUrl: getUrl,   
        minDate: minDate,
    };


}());