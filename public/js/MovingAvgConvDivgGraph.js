
//Moving Averge convergence/Divergence Graph module for stock  
//Author: Munish Malhotra

//NOTE: Moving average convergence divergence (MACD) is a trend-following momentum indicator that shows the relationship 
//between two moving averages of prices
//Read more: Moving Average Convergence Divergence (MACD) 
//https://www.investopedia.com/terms/m/macd.asp#ixzz5FbA5zeLb 

//For more detail algorithmic implementation, please refer:
//http://stockcharts.com/school/doku.php?id=chart_school:technical_indicators:moving_average_convergence_divergence_macd


//========================================================================================
//                                  PRIVATE METHODS
//========================================================================================
var MovingAvgConvDivgGraph = (function () {
    'use strict';
    var DOM = {};

    // cache DOM elements for internal use
    function cacheDom() {
        DOM.$symbol = $('#StockSymbol').val().toUpperCase();
        DOM.$ctx = document.getElementById('MACD-Chart').getContext("2d");
        DOM.$startDate = $("#StartDate");
        DOM.$MACDChart = $("#MACD-ChartDiv");
        DOM.$goButton = $("#btnLoadData");

        //UI Text Elem
        DOM.$companyCurrentInfo = $('#CurrentInfo');
        DOM.$companyName = $('#CurrentcompanyName');
        DOM.$sellTicker = $('#BuySellMACD');
    }

    function reset() {
        DOM.$MACDChart.hide();
    }

    //hack
    function disableGoButtonTillLoadingIsImplemented() {
        DOM.$goButton
            .attr("disabled", "disabled");
    }


    //get the MACD data
    function getSymbolMACDData() {
        var api = {
            //endpoint: 'https://www.alphavantage.co/query',
            endpoint: Utils.getUrl(),
            params: {
                'function': 'MACD',
                'symbol': DOM.$symbol,
                'interval': 'daily',
                'series_type': 'close',
                'fastperiod': '10',
                'apikey': 'HZGSAGH6VAAO71CN'
            }
        };

        $.getJSON(api.endpoint, api.params)
            .then(populate)
            .then(getStockDailyData)
            .fail(handleError);
    }

    // handle errors
    function handleError(err) {
        // Hide something 
        console.log(err);
    }

    function getStockDailyData(result) {
        var api = {
            //endpoint: 'https://www.alphavantage.co/query',
            endpoint: Utils.getUrl(),
            params: {
                'function': 'TIME_SERIES_DAILY',
                'symbol': DOM.$symbol,
                'apikey': 'HZGSAGH6VAAO71CN'
            }
        };

        $.getJSON(api.endpoint, api.params)
            .then(data => populateDailyChartData(data, result))
            .then(render)
            .fail(handleError);
    }

    function populateDailyChartData(response, chartData) {
        let startVal = Utils.minDate(chartData.labels);
        $.each(response['Time Series (Daily)'], function (i, obj) {
            var DT = moment(i).format("YYYY-MM-DD");
            var SV = moment(startVal).format("YYYY-MM-DD");
            // Only push values if the date is greater than equal to startVal
            if (moment(DT).isSameOrAfter(moment(SV))) {
                var MACDval = obj['4. close'];
                chartData.datasets[0].data.push(MACDval);
            }
        });

        // Reverse Chart
        chartData.labels.reverse();
        chartData.datasets[0].data.reverse(); //Stock
        chartData.datasets[1].data.reverse(); //MACD
        chartData.datasets[2].data.reverse(); //Signal
        //chartData.datasets[3].data.reverse(); //Histogram
        return chartData;
    }

    //Populate data coming from API
    function populate(response) {
        let chartData = {
            labels: [],
            datasets: [{
                type: 'line',
                label: DOM.$symbol + ' Daily Stock',
                id: "y-axis-0",
                backgroundColor: "rgba(217,83,79,0.75)",
                borderColor: "rgba(217,83,79,0.75)",
                yAxisID: 'y-axis-0',
                fill: false,
                data: []
            }, {
                type: 'line',
                label: 'MACD',
                id: "y-axis-1",
                backgroundColor: "rgba(0, 255, 0, 0.3)",
                borderColor: "rgba(0, 255, 0, 0.3)",
                yAxisID: 'y-axis-1',
                data: []
            }, {
                type: 'line',
                label: 'MACD_Signal',
                id: "y-axis-1",
                backgroundColor: "rgba(255, 0, 0, 0.3)",
                borderColor: "rgba(255, 0, 0, 0.3)",
                yAxisID: 'y-axis-1',
                data: []
            }]
        };


        $.each(response['Technical Analysis: MACD'], function (i, obj) {
            var DT = moment(i).format('MM-DD-YYYY');
            var TenDayDT = (moment()).subtract(10, 'day').format('MM-DD-YYYY');
            // Push values under 10 days
            if (moment(DT).isSameOrAfter(moment(TenDayDT))) {
                chartData.labels.push(moment(Date.parse(DT)).format('M-DD-YYYY'));
                var MACDval = obj.MACD;
                chartData.datasets[1].data.push(MACDval)     // MACD
                var MACDSignal = obj.MACD_Signal;
                var MACDHist = obj.MACDHist;
                chartData.datasets[2].data.push(MACDSignal); //Signal
            }
        });
        return chartData;

    }

    function render(MACDData) {
        if (window.lineMACDChart != null) //Charts.js ERROR https://github.com/chartjs/Chart.js/issues/3753
            window.lineMACDChart.destroy(); // destroy the old chart else overlay issue on mouse hover 

        if (!MACDData || MACDData.labels.length === 0) {
            toastr.error("Unable to fetch details, check symbol again!", { timeout: 2000 });
            DOM.$goButton
            .removeAttr("disabled");
            return;
        }

        window.lineMACDChart = new Chart(DOM.$ctx, {
            type: 'bar',
            data: MACDData,
            options: {
                title: {
                    display: true,
                    text: "Moving Average Convergence/Divergence (MACD)"
                },
                tooltips: {
                    mode: 'label'
                },
                responsive: true,
                scales: {
                    xAxes: [{
                        stacked: false
                    }],
                    yAxes: [{
                        beginAtZero: true,
                        stacked: true,
                        position: "left",
                        id: "y-axis-0",
                        display: true,
                        labels: {
                            show: true,
                        }
                    }, {
                        //stacked: true,
                        position: "right",
                        id: "y-axis-1",
                        type: 'linear',
                        display: true,
                        labels: {
                            show: true,
                        }
                    }]
                }
            }
        });

        //Make chart visible on DOM 
        DOM.$MACDChart.show();
        DOM.$goButton
            .removeAttr("disabled");

        //Make UI changes depending upon Bullish and Bearish behavior
        updateStockUIBehavior(shouldBuyStock(MACDData));

    };

    //return true when stock performing good
    //though this is not the best signal to predict bear vs bull nature of stock
    //but for MVP purposes this should be good enough.
    function shouldBuyStock(MACDData) {
        // If MACD is higher than Signal Line then it epicts bullish behavior
        let MACDArray = MACDData.datasets[1].data //MACD
        let SignalArray = MACDData.datasets[2].data; //Signal

        //TODO: check for type casting voilations
        var lastMACDValue = Number(MACDArray[MACDArray.length - 1]);
        var lastSingalValue = Number(SignalArray[SignalArray.length - 1]);

        if (lastMACDValue > lastSingalValue) {
            return true;
        }
        return false;
    }

    //TODO: UI should be handled in the UI modules only.
    // Update the stock Color UI based on Bullish/Bearish
    function updateStockUIBehavior(shouldBuy) {
        if (shouldBuy) {
            // Buy
            DOM.$sellTicker
                .addClass('panel-success')
                .removeClass('panel-danger');

            $('#BuySellMACD .panel-heading')
                .text('Stats are favourable, you can buy this');
            $('#BuySellMACD .panel-body')
                .text('Your MACD is currently higher than the signal. This signals bullish behavior');

            DOM.$companyCurrentInfo
                .css("color", "green");

            DOM.$companyName
                .css("color", "green");

            //TODO: Notifications should be wrapped inside a different module 
            toastr.success('You can buy this stock', 'Market Guru', { timeOut: 3500 });
        }
        else {
            // Sell
            DOM.$sellTicker
                .addClass('panel-danger')
                .removeClass('panel-success');

            $('#BuySellMACD .panel-heading')
                .text('Keep a tight eye on it, you can sell this');

            $('#BuySellMACD .panel-body')
                .text('Your MACD is currently lower than the signal. This signals bearish behavior');

            DOM.$companyCurrentInfo
                .css("color", "red");

            DOM.$companyName
                .css("color", "red");
            //TODO: Notifications should be wrapped inside a different module 
            toastr.error('Stock is inclined to bearish behavior', 'Market Guru', { timeOut: 3500 });
        }
    }

    //========================================================================================
    //                                  PUBLIC METHODS
    //========================================================================================

    function init() {
        cacheDom();
        reset();
        disableGoButtonTillLoadingIsImplemented();
        getSymbolMACDData();
    }

    //========================================================================================
    //                                  EXPORT PUBLIC METHODS
    //========================================================================================

    return {
        init: init,
        shouldBuyStock: shouldBuyStock //export for testing only
    };

}());
