//Author: Munish Malhotra

// UI elements representing the company relataed information on DOM

var CompanyInfo = (function () {
  'use strict';
  var DOM = {};

  //========================================================================================
  //                                  PRIVATE METHODS
  //========================================================================================
  // cache DOM elements
  function cacheDom() {
    DOM.$symbol = $('#StockSymbol').val().toUpperCase();
    DOM.$companyLogo = $("#CompanyLogo");
    DOM.$currentInfo = $('#CurrentInfo');
  }

  

  function renderCompanyStockDetails(response) {
    $("#CurrentLastDate").text("" + moment(response.latestUpdate).format('LLL'));
    $("#CurrentPrice").text("" + response.latestPrice);
    $("#CurrentHigh").text("" + (response.high == null) ? "" : response.high  );
    $("#CurrentLow").text("" + (response.low == null) ? "" : response.high );
    $("#Current52Low").text("" + response.week52Low);
    $("#Current52High").text("" + response.week52High);
    $("#CurrentcompanyName").text("" + response.companyName);

    $("#CurrentInfo").show();
  }

  function getCompanyStockDetails() {
    var api = {
      endpoint: 'https://api.iextrading.com/1.0/stock/' + DOM.$symbol + "/quote",
      params: {}
    };
    $.getJSON(api.endpoint, api.params)
      .then(renderCompanyStockDetails)
      .fail(handleStockError);
  }

  // handle errors
  function handleStockError(err) {
    console.log(err);
  }

  function getCompanyLogo() {
    var api = {
      endpoint: 'https://api.iextrading.com/1.0/stock/' + DOM.$symbol + "/logo",
      params: {}
    };
    $.getJSON(api.endpoint, api.params)
      .then(renderCompanyLogo)
      .fail(handleError);
  }

  // handle errors
  function handleError(err) {
    DOM.$companyLogo.hide();
    console.log(err);
  }

  //Reset UI elements information before the new call.
  function resetDomUIElements() {
    $("#CurrentLastDate").text("");
    $("#CurrentPrice").text("");
    $("#CurrentHigh").text("");
    $("#CurrentLow").text("");
    $("#Current52Low").text("");
    $("#Current52High").text("");
    $("#CurrentcompanyName").text(""); 
  }

  //render company logo
  function renderCompanyLogo(response) {
    DOM.$companyLogo
      .attr("src", response.url)
      .show()
  }


  //========================================================================================
  //                                  PUBLIC METHODS
  //========================================================================================

  function init() {
    cacheDom();
    resetDomUIElements();
    getCompanyLogo();
    getCompanyStockDetails();

  }

  //========================================================================================
  //                                  EXPORT PUBLIC METHODS
  //========================================================================================

  return {
    init: init
  };
}());