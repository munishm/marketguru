/* /js/stockLineGraph.js */
//Stock Line Graph module for weekly stock chart 
//Author: Munish Malhotra


var stockLineGraph = (function () {
    'use strict';
    var DOM = {};

    //========================================================================================
    //                                  PRIVATE METHODS
    //========================================================================================

    // cache DOM elements for internal use
    function cacheDom() {
        DOM.$symbol = $('#StockSymbol').val().toUpperCase();
        DOM.$startDate = $("#StartDate");
        DOM.$sellTicker = $('#BuySell');
        DOM.$lineChart = $("#Weekly-ChartDiv");
        DOM.$ctx = document.getElementById('line-chart').getContext("2d");
    }

    function reset(){
        DOM.$lineChart.hide();
    }


    // get random quote
    function getSymbolWeeklyData() {
        var api = {
            //endpoint: 'https://www.alphavantage.co/query',
            endpoint: Utils.getUrl(),
            params: {
                'function': 'TIME_SERIES_WEEKLY',
                'symbol': DOM.$symbol,
                'apikey': 'HZGSAGH6VAAO71CN'
            }
        };

        $.getJSON(api.endpoint, api.params)
            .then(populate)
            .then(render)
            .fail(handleError);
    }

    // handle errors
    function handleError(err) {
        // Hide something 
        console.log(err);
    }

    function populate(response) {
        console.log("Transforming the response");
        //Construct the line graph options
        let chartData = {
            labels: [],
            datasets: [{
                label: 'Weekly Close Price',
                backgroundColor: "#0084ff",
                pointBorderColor: "#0084ff",
                pointBackgroundColor: "#0084ff",
                pointHoverBackgroundColor: "#0084ff",
                pointHoverBorderColor: "#0084ff",
                pointBorderWidth: 10,
                pointHoverRadius: 10,
                pointHoverBorderWidth: 1,
                pointRadius: 3,
                fill: false,
                borderWidth: 4,
                data: [],
                hoverBackgroundColor: []
            }]
        };


        //response['Weekly Time Series'].each(function () {  });

        $.each(response['Weekly Time Series'], function (i, ob) {
            //var labels = $(this);
            var startDate = Date.parse($("#StartDate").val());
            var weekOf = Date.parse(i);
            if (weekOf > startDate) {
                chartData.labels.push(moment(weekOf).format('MM/DD/YYYY'));
                var closePrice = ob['4. close'];
                chartData.datasets[0].data.push(closePrice);
            }
        });
        // Reverse
        chartData.labels.reverse();
        chartData.datasets[0].data.reverse();
        return chartData;
    };

    //var stockLineChart = null;

    //render
    function render(chartData) {

        if (window.stockLineChart != null) - //Charts.js ERROR https://github.com/chartjs/Chart.js/issues/3753s
            window.stockLineChart.destroy(); // destroy the old chart else overlay issue on mouse hover 

        //ctx properties

        //Construct options    
        window.stockLineChart = new Chart(DOM.$ctx, {
            type: 'line',
            data: chartData,
            options: {
                responsive: true,
                scales: {
                    yAxes: [{
                        ticks: {
                            fontColor: "rgba(0,0,0,0.5)",
                            fontStyle: "bold",
                            beginAtZero: false,
                            maxTicksLimit: 5,
                            padding: 20
                        },
                        gridLines: {
                            drawTicks: false,
                            display: false
                        }

                    }],
                    xAxes: [{
                        gridLines: {
                            zeroLineColor: "transparent"

                        },
                        ticks: {
                            padding: 20,
                            fontColor: "rgba(0,0,0,0.5)",
                            fontStyle: "bold"
                        }
                    }]
                }
            },
            animation: {
                animateScale: false
            }
        });
        DOM.$lineChart.show();
    }

    //========================================================================================
    //                                  PUBLIC METHODS
    //========================================================================================

    function init() {
        cacheDom();
        reset();
        getSymbolWeeklyData();
    }

    //========================================================================================
    //                                  EXPORT PUBLIC METHODS
    //========================================================================================
    return {
        init: init
    };
}());