/* /js/goButton.js */

//Author: Munish Malhotra

var UIBindings = (function () {
    'use strict';
    var DOM = {};

//========================================================================================
//                                  PRIVATE METHODS
//========================================================================================
    // cache DOM elements
    function cacheDom() {
      DOM.$goButton =  $("#btnLoadData"); 
    }

    function bindEvents(){
      DOM.$goButton.on('click', function(){
        clickButtonHandler('go');
      });
    }
   
    function clickButtonHandler(button) {
      //Refresh DOM elements

      CompanyInfo.init();
      stockLineGraph.init();
      MovingAvgConvDivgGraph.init();
    }
    
//========================================================================================
//                                  PUBLIC METHODS
//========================================================================================

function init() {
  cacheDom();
  bindEvents();
  }

//========================================================================================
//                                  EXPORT PUBLIC METHODS
//========================================================================================

 return {
    init: init
  };


}());