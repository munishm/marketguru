const express = require('express'),
    app = express(),
    path = require('path'),
    expressValidator = require('express-validator'),
    mongoose = require('mongoose'),
    http = require('http').Server(app);

const { check, validationResult } = require('express-validator/check');
const stock = require('./server/models/stock');
const config = require('./config')
const request = require('request');
const axios = require('axios');

//db options
let options = {
    server: { socketOptions: { keepAlive: 1, connectTimeoutMS: 30000 } },
    replset: { socketOptions: { keepAlive: 1, connectTimeoutMS: 30000 } }
};

//db connection      
mongoose.connect(config.DBHost, {
    auth:{
        user: config.mongo.username,
        password: config.mongo.password 
    }
});
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

app.use(express.static(path.join(__dirname, './public')));

app.get('/teststock', function (req, res) {
    //Creates a new stock
    let testStock = {
        "user": "munish",
        "stock": "MSFT",
        "stockDetails": { "abc": "def", "ghi": "klo" }
    }
    var newStock = new stock(testStock);
    console.log(newStock);

    newStock.save((err, stock) => {
        if (err) {
            res.send(err);
        }
        else { //If no errors, send it back to the client
            res.json({ message: "Stock successfully added!", stock });
        }
    });

});

app.get('/', function (req, res) {
    console.log("in defaualt get");
    res.render('./public/index.html');
    //res.sendFile('./public/index.html', { root: __dirname });
});

app.get('/stock', [
    check('function').not().isEmpty(),
    check('symbol').not().isEmpty(),
    check('apikey').not().isEmpty()
],
    function (req, res) {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({ errors: errors.array() });
        }
        //Get request parameters
        let queryFunction = req.query.function,
            symbol = req.query.symbol,
            apikey = req.query.apikey;

        let url;
        switch (queryFunction) {
            case 'TIME_SERIES_DAILY':
                url = 'https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol='+symbol+'&apikey=8IKZ0271NT37IM7R';
                break;
            case 'TIME_SERIES_WEEKLY':
                url = 'https://www.alphavantage.co/query?function=TIME_SERIES_WEEKLY&symbol='+symbol+'&apikey=8IKZ0271NT37IM7R';
                break;
            case 'MACD':
                url = 'https://www.alphavantage.co/query?function=MACD&symbol='+symbol+'&interval=daily&series_type=close&fastperiod=10&apikey=8IKZ0271NT37IM7R';
                break;    
        }
        console.log(url);

        axios.get(url)
            .then(response => {
                //TODO: Currently these API is only acting as a prxoy for aplhavantage 
                //We should parse response and compose our own success and error response
                //Should also implement the fallback API incase aplhavantage is out of service.
                
                
                //save it to db
                //Saving all 3 API's data into persistence storage for later analysis
                var newStock = new stock({
                    "user": "munish",
                    "stock": "MSFT", 
                    "stockDetails": response.data
                });
                newStock.save((err, stock) => {
                    if (err) {
                        res.send(err);
                    }
                    else { //If no errors, send it back to the client
                        res.send(response.data);
                    }
                });
            })
            .catch(error => {
                console.log(error);
            });

    });

http.listen(3001, function () {
    console.log('listening on: 3001');
});

module.exports = app; // for testing