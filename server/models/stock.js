let mongoose = require('mongoose');
let Schema = mongoose.Schema;

//stock schema definition
let StockSchema = new Schema(
    {
      user: { type: String, required: true },
      stock: { type: String, required: true },
      stockDetails: Object,
      company: { type: Number, required: false },
      createdAt: { type: Date, default: Date.now },    
    }, 
    { 
      versionKey: false
    }
  );
  
  // Sets the createdAt parameter equal to the current time
  StockSchema.pre('save', next => {
    now = new Date();
    if(!this.createdAt) {
      this.createdAt = now;
    }
    next();
  });
  
  //Exports the StockSchema for use elsewhere.
  module.exports = mongoose.model('stock', StockSchema);