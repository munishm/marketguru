# Market Guru

Market Guru is a cloud-enabled HTML5, node-js application which lets you check the stock symbol latest price, trends and recomendation.

 - Get the current price, Today's High and low
 - Get the 52 Week Low and high of stock
 - See the weekly trend from last year
 - See the MACD & singal charts for the stock
 - Get the recommendation as a notification as well

# Tech!
> MarkerGuru uses HTML5 with node.js backend and for unit testing uses Mocha framework and chai.js. 

Market Guru uses few open source projects

* [node.js](http://node.js) - API backend
* [mongoDb](http://mongodb.com) - backend database
* [Express] - app framework
* [axios](https://github.com/axios/axios) - Promise based httpClient 
* [Mongoose] - ORM for Mongo
* [Mocha] - Unit test framework
* [Gulp] - Build system
* [npm] - because its nice to run your scripts through it.

### Installation

Market Guru requires [Node.js](https://nodejs.org/) to run.

These commands will install the dependencies and start the server.

```sh
$ cd MarketGuru
$ npm install 
$ npm start
```

To run tests...

```sh
cd MarketGuru
$ npm test
```

### TODO
* Loading bar is missing big time, must have!
* Minification & unification has not been done deliberately. 
* Incline CSS is included deliberately. 
* Solution is not tested on Mobile phones or all browsers
* Apache ab/any benchmarking has not been done
* Test coverage is not 100% complete
* Swagger could have been used for API stubs
* No inherent security implementation
    * can use JWT for security
    * server side security to be implemented
    * API throttling should be incorporated
* Mockgoose can be implemented for proxing Mongoose for unit testing
* Simple moving average can be implemented
    * Buy/Sell recommendation weightage to be given not only on MACD vs signal but to other parameters as well
* May be a better UI represenatation for buy/sell recommendation like Gauge.
* Push to Azure deployment git.
